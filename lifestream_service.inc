<?php
// $Id:

/**
 * Get lifestream entries for a given username. 
 *
 * @param $username
 *   String. Username
 * @return
 *   Array. All elements of lifestream. 
 */
function lifestream_service_getalife($userid) {
  $i = 0; 

  //the stream for a given user id. 
  //$lifestream = db_query('SELECT * FROM {activitystream} AS a, {node} AS n WHERE a.nid = n.nid AND n.uid=%s', $userid);
  $lifestream = db_query('SELECT n.title, n.nid, s.module, s.link, s.data, n.created FROM {activitystream} s, {node} n WHERE s.nid=n.nid AND n.status =1 AND n.uid = %s ORDER BY n.created DESC', $userid); 	

  //TODO: this needs to be improved. 
  $entries = array(); 
  while ($node = db_fetch_object($lifestream)) {
	$entries[$i] = $node;
	$i++;  
  }
  
  return $entries; 
}


/**
 * Check if the user has permission to get a given lifestream
 */
function lifestream_service_get_access() {
  global $user;
  
  if (user_access('get all lifestream data')) {
    return TRUE;
  }
}


?>